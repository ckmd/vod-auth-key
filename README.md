## 点播访问控制URl鉴权

###安装

composer require ckmd/vod-auth-key

###Demo

//key由阿里云账号管理员提供 获取路径 点播控制台->分发加速配置->域名管理->访问控制->URL鉴权

//这个参数建议写在配置文件里

$key = "aaaaaaaaaaaaaaaaaa";

//这里是视频地址（注意不带域名；标清和高清地址是不同的地址）

$url = "/e45465885a344695a1ba9c907e3cbd7e/aa3c570ee88d4a538f988fb5da19bffe-eea3b031a29a0645e2f5bdc8e4103f16-ld.m3u8";

$vod = new VodAuthKey($key);

$url = $vod->getAuthKeyUrl($url);
